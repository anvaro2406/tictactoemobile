import 'package:flutter/material.dart';
import 'package:regexpattern/regexpattern.dart';
import 'package:google_fonts/google_fonts.dart';
import 'httpRequests.dart';
import 'drawer.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  late String errorMessage = "";
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        drawer: const StartDrawer(),
        body: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                    child: Center(
                      child: Text('Sign in',
                          style: GoogleFonts.playfairDisplay(
                              textStyle: const TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(246, 238, 238, 1.0)))),
                    )),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                          width: MediaQuery.of(context).size.width * 0.80,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(27, 29, 44, 1.0)),
                          child: TextFormField(
                            controller: usernameController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter username';
                              } else if (!value.isAlphabet()) {
                                return 'Please use only letters';
                              }
                              return null;
                            },
                            style: const TextStyle(
                                fontSize: 22,
                                color: Color.fromRGBO(246, 238, 238, 1.0)),
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: Color.fromRGBO(246, 238, 238, 1.0)),
                              labelText: 'Username',
                            ),
                          )),
                      Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                          width: MediaQuery.of(context).size.width * 0.80,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(27, 29, 44, 1.0)),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter password';
                              } else if (value.length < 8) {
                                return 'Use more than 8 characters';
                              } else if (!value.isPasswordEasy()) {
                                return 'Please use any characters except whitespace.';
                              }
                              return null;
                            },
                            style: const TextStyle(
                                fontSize: 22,
                                color: Color.fromRGBO(246, 238, 238, 1.0)),
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: Color.fromRGBO(246, 238, 238, 1.0)),
                              labelText: 'Password',
                            ),
                          )),
                    ],
                  ),
                ),
                OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      signInRequest(
                        usernameController.text,
                        passwordController.text,
                      ).then((value) => (value == "")
                          ? Navigator.pushNamed(context, "/account",
                                  arguments: usernameController.text)
                              .then((value) => setState(() {
                                    usernameController.text = "";
                                    passwordController.text = "";
                                  }))
                          : setState(() {
                              errorMessage = value;
                            }));
                    }
                  },
                  style: OutlinedButton.styleFrom(
                      minimumSize: const Size(150, 40),
                      backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
                  child: const Text('Continue',
                      style: TextStyle(
                          fontSize: 18,
                          color: Color.fromRGBO(246, 238, 238, 1.0))),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Center(
                      child: Text(errorMessage,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(255, 0, 0, 1.0)))),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: OutlinedButton(
          style: OutlinedButton.styleFrom(
              minimumSize: const Size(50, 50),
              backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
          onPressed: () {
            _key.currentState!.openDrawer();
          },
          child: const Icon(
            Icons.navigation_outlined,
            color: Color.fromRGBO(246, 238, 238, 1.0),
            size: 30,
          ),
        ),
        backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
  }
}

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  late String errorMessage = "";
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        drawer: const StartDrawer(),
        body: Center(
            child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                  child: Center(
                    child: Text('Sign up',
                        style: GoogleFonts.playfairDisplay(
                            textStyle: const TextStyle(
                                fontSize: 36,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(246, 238, 238, 1.0)))),
                  )),
              Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                        width: MediaQuery.of(context).size.width * 0.80,
                        decoration: const BoxDecoration(
                            color: Color.fromRGBO(27, 29, 44, 1.0)),
                        child: TextFormField(
                          controller: usernameController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter username';
                            } else if (!value.isAlphabet()) {
                              return 'Please use only letters';
                            }
                            return null;
                          },
                          style: const TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(246, 238, 238, 1.0)),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelStyle: TextStyle(
                                fontSize: 20,
                                color: Color.fromRGBO(246, 238, 238, 1.0)),
                            labelText: 'Username',
                          ),
                        )),
                    Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                        width: MediaQuery.of(context).size.width * 0.80,
                        decoration: const BoxDecoration(
                            color: Color.fromRGBO(27, 29, 44, 1.0)),
                        child: TextFormField(
                          obscureText: true,
                          controller: passwordController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter password';
                            } else if (value.length < 8) {
                              return 'Use more than 8 characters';
                            } else if (!value.isPasswordEasy()) {
                              return 'Please use any characters except whitespace.';
                            }
                            return null;
                          },
                          style: const TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(246, 238, 238, 1.0)),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelStyle: TextStyle(
                                fontSize: 20,
                                color: Color.fromRGBO(246, 238, 238, 1.0)),
                            labelText: 'Password',
                          ),
                        )),
                  ])),
              OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    signUpRequest(
                      usernameController.text,
                      passwordController.text,
                    ).then((value) => (value == "")
                        ? Navigator.pushNamed(context, "/account",
                                arguments: usernameController.text)
                            .then((value) => setState(() {
                                  usernameController.text = "";
                                  passwordController.text = "";
                                }))
                        : setState(() {
                            errorMessage = value;
                          }));
                  }
                },
                style: OutlinedButton.styleFrom(
                    minimumSize: const Size(150, 40),
                    backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
                child: const Text('Continue',
                    style: TextStyle(
                        fontSize: 18,
                        color: Color.fromRGBO(246, 238, 238, 1.0))),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Center(
                    child: Text(errorMessage,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 18,
                            color: Color.fromRGBO(255, 0, 0, 1.0)))),
              ),
            ],
          ),
        )),
        floatingActionButton: OutlinedButton(
          style: OutlinedButton.styleFrom(
              minimumSize: const Size(50, 50),
              backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
          onPressed: () {
            _key.currentState!.openDrawer();
          },
          child: const Icon(
            Icons.navigation_outlined,
            color: Color.fromRGBO(246, 238, 238, 1.0),
            size: 30,
          ),
        ),
        backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
  }
}
