import 'dart:math';
import 'package:flutter/material.dart';

class GameModel extends ChangeNotifier {
  bool playerMarkType = Random().nextBool(); // true - X, false - O
  bool markTypeToMove = true; // true - X, false - O
  late String username; // true - X, false - O
  List<List<String>> squares = [
    ["", "", ""],
    ["", "", ""],
    ["", "", ""]
  ];
  List<String> logs = [];

  String getMarkByCoord(int row, int col) {
    return squares[row][col];
  }

  void makePlayerMove(int row, int col) {
    if (markTypeToMove) {
      squares[row][col] = "X";
    } else {
      squares[row][col] = "O";
    }
    notifyListeners();
  }

  Map<int, int> makeComputerMove() {
    for (int i = 0; i < squares.length; i++) {
      for (int j = 0; j < squares[0].length; j++) {
        if (squares[i][j] == "") {
          squares[i][j] = (markTypeToMove) ? "X" : "O";
          notifyListeners();
          return {i: j};
        }
      }
    }
    return {-1: -1};
  }

  bool isFirstMove() {
    if (squares[0][0] == "" &&
        squares[0][1] == "" &&
        squares[0][2] == "" &&
        squares[1][0] == "" &&
        squares[1][1] == "" &&
        squares[1][2] == "" &&
        squares[2][0] == "" &&
        squares[2][1] == "" &&
        squares[2][2] == "") {
      return true;
    }
    return false;
  }

  // "X" if X win, "O" if O win, D if draw, "" else
  String isWin() {
    if (squares[0][0] == squares[0][1] &&
        squares[0][1] == squares[0][2] &&
        squares[0][0] != "") {
      return (squares[0][0] == "X") ? "X" : "O";
    } else if (squares[1][0] == squares[1][1] &&
        squares[1][1] == squares[1][2] &&
        squares[1][0] != "") {
      return (squares[1][0] == "X") ? "X" : "O";
    } else if (squares[2][0] == squares[2][1] &&
        squares[2][1] == squares[2][2] &&
        squares[2][0] != "") {
      return (squares[2][0] == "X") ? "X" : "O";
    } else if (squares[0][0] == squares[1][0] &&
        squares[1][0] == squares[2][0] &&
        squares[0][0] != "") {
      return (squares[0][0] == "X") ? "X" : "O";
    } else if (squares[0][1] == squares[1][1] &&
        squares[1][1] == squares[2][1] &&
        squares[0][1] != "") {
      return (squares[0][1] == "X") ? "X" : "O";
    } else if (squares[0][2] == squares[1][2] &&
        squares[1][2] == squares[2][2] &&
        squares[0][2] != "") {
      return (squares[0][2] == "X") ? "X" : "O";
    } else if (squares[0][0] == squares[1][1] &&
        squares[1][1] == squares[2][2] &&
        squares[0][0] != "") {
      return (squares[0][0] == "X") ? "X" : "O";
    } else if (squares[0][2] == squares[1][1] &&
        squares[1][1] == squares[2][0] &&
        squares[0][2] != "") {
      return (squares[0][2] == "X") ? "X" : "O";
    } else if (squares[0][0] != "" &&
        squares[0][1] != "" &&
        squares[0][2] != "" &&
        squares[1][0] != "" &&
        squares[1][1] != "" &&
        squares[1][2] != "" &&
        squares[2][0] != "" &&
        squares[2][1] != "" &&
        squares[2][2] != "") {
      return "D";
    } else {
      return "";
    }
  }

  void changeMarkTypeToMove() {
    markTypeToMove = !markTypeToMove;
    notifyListeners();
  }

  void setUsername(String name) {
    username = name;
  }

  void log(String newLogLine) {
    logs.add(newLogLine);
    notifyListeners();
  }

  String getLogsInString() {
    String tmp = "";
    int rowNumber = 1;
    for (var logLine in logs) {
      tmp += "$rowNumber: " + logLine + "\n";
      rowNumber++;
    }
    return tmp;
  }

  bool isPlayerMarkX() {
    return playerMarkType;
  }

  bool isPlayerMarkO() {
    return !playerMarkType;
  }

  bool isMarkToMoveX() {
    return markTypeToMove;
  }

  bool isMarkToMoveO() {
    return !markTypeToMove;
  }
}