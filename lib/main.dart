import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'sign_in_up.dart';
import 'account.dart';
import 'game.dart';
import 'GameModel.dart';
import 'drawer.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => StartPage(),
          '/sign_in': (context) => const SignInPage(),
          '/sign_up': (context) => const SignUpPage(),
          '/account': (context) => const AccountPage(),
          '/game': (context) => ChangeNotifierProvider(
                create: (context) => GameModel(),
                child: GamePage(),
              ),
        });
  }
}

class StartPage extends StatelessWidget {
  StartPage({Key? key}) : super(key: key);
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        drawer: const StartDrawer(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Welcome to',
                  style: GoogleFonts.playfairDisplay(
                      textStyle: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(246, 238, 238, 1.0)))),
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 50.0),
                  child: Center(
                    child: Text('Tic-tac-toe',
                        style: GoogleFonts.playfairDisplay(
                            textStyle: const TextStyle(
                                fontSize: 36,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(246, 238, 238, 1.0)))),
                  )),
              OutlinedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/sign_up');
                },
                style: OutlinedButton.styleFrom(
                    minimumSize: const Size(150, 40),
                    backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
                child: const Text('Sign up',
                    style: TextStyle(
                        fontSize: 18,
                        color: Color.fromRGBO(246, 238, 238, 1.0))),
              ),
              OutlinedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/sign_in');
                },
                style: OutlinedButton.styleFrom(
                    minimumSize: const Size(150, 40),
                    backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
                child: const Text('Sign in',
                    style: TextStyle(
                        fontSize: 18,
                        color: Color.fromRGBO(246, 238, 238, 1.0))),
              ),
            ],
          ),
        ),
        floatingActionButton: OutlinedButton(
          style: OutlinedButton.styleFrom(
              minimumSize: const Size(50, 50),
              backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
          onPressed: () {
            _key.currentState!.openDrawer();
          },
          child: const Icon(
            Icons.navigation_outlined,
            color: Color.fromRGBO(246, 238, 238, 1.0),
            size: 30,
          ),
        ),
        backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
  }
}
