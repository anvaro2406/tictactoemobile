import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'httpRequests.dart';
import 'GameModel.dart';
import 'drawer.dart';

class GamePage extends StatelessWidget {
  GamePage({Key? key}) : super(key: key);
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final username = ModalRoute.of(context)!.settings.arguments as String;
    const fontSizeCoeff = 0.75;
    String startMark =
        (Provider.of<GameModel>(context, listen: false).isPlayerMarkX())
            ? "X"
            : "O";
    Provider.of<GameModel>(context, listen: false).setUsername(username);

    if (Provider.of<GameModel>(context, listen: false).isMarkToMoveX() !=
            Provider.of<GameModel>(context, listen: false).playerMarkType &&
        Provider.of<GameModel>(context, listen: false).isFirstMove()) {
      String mark =
          Provider.of<GameModel>(context, listen: false).isMarkToMoveX()
              ? "X"
              : "O";
      Map<int, int> computerMoveCoord =
          Provider.of<GameModel>(context, listen: false).makeComputerMove();
      Provider.of<GameModel>(context, listen: false).log(
          "Log info: $mark go to [" +
              computerMoveCoord.keys.elementAt(0).toString() +
              ";" +
              computerMoveCoord.values.elementAt(0).toString() +
              "]");
      String isWinVar = Provider.of<GameModel>(context, listen: false).isWin();
      if (isWinVar != "") {
        afterEndGameActions(isWinVar, context);
      } else {
        Provider.of<GameModel>(context, listen: false).changeMarkTypeToMove();
      }
    }

    return Scaffold(
        key: _key,
        drawer: SignInDrawer(
          username: username,
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Center(
                        child: Text(username + ' vs Computer',
                            style: GoogleFonts.playfairDisplay(
                                textStyle: const TextStyle(
                                    fontSize: 36,
                                    fontWeight: FontWeight.bold,
                                    color:
                                        Color.fromRGBO(246, 238, 238, 1.0)))),
                      )),
                  Container(
                      margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Center(child: Consumer<GameModel>(
                        builder: (context, game, child) {
                          return Text(
                              'Your mark is ' +
                                  startMark +
                                  "\nThe current move is " +
                                  ((game.markTypeToMove) ? "X" : "O"),
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(246, 238, 238, 1.0)));
                        },
                      ))),
                  const Board(),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.90,
                      height: MediaQuery.of(context).size.height * 0.18,
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(27, 29, 44, 1.0)),
                      child: Consumer<GameModel>(
                        builder: (context, game, child) {
                          return SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Text(game.getLogsInString(),
                                  style: const TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      color:
                                          Color.fromRGBO(246, 238, 238, 1.0))));
                        },
                      )),
                ],
              ),
            );
          } else {
            return Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                  width:
                                      MediaQuery.of(context).size.width * 0.4,
                                  alignment: Alignment.centerRight,
                                  child: Center(
                                    child: Text(username + ' vs Computer',
                                        style: GoogleFonts.playfairDisplay(
                                            textStyle: const TextStyle(
                                                fontSize: 36 * fontSizeCoeff,
                                                fontWeight: FontWeight.bold,
                                                color: Color.fromRGBO(
                                                    246, 238, 238, 1.0)))),
                                  )),
                              const Board(
                                squareSideLength: 0.25,
                              ),
                            ]),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width * 0.4,
                                margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Center(child: Consumer<GameModel>(
                                  builder: (context, game, child) {
                                    return Text(
                                        'Your mark is ' +
                                            startMark +
                                            "\nThe current move is " +
                                            ((game.markTypeToMove) ? "X" : "O"),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontSize: 22 * fontSizeCoeff,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromRGBO(
                                                246, 238, 238, 1.0)));
                                  },
                                ))),
                            Container(
                                width: MediaQuery.of(context).size.width * 0.4,
                                height:
                                    MediaQuery.of(context).size.height * 0.75,
                                decoration: const BoxDecoration(
                                    color: Color.fromRGBO(27, 29, 44, 1.0)),
                                child: Consumer<GameModel>(
                                  builder: (context, game, child) {
                                    return SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Text(game.getLogsInString(),
                                            style: const TextStyle(
                                                fontSize: 22 * fontSizeCoeff,
                                                fontWeight: FontWeight.bold,
                                                color: Color.fromRGBO(
                                                    246, 238, 238, 1.0))));
                                  },
                                )),
                          ],
                        ),
                      ],
                    )
                  ]),
            );
          }
        }),
        backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
  }
}

class Board extends StatelessWidget {
  const Board({Key? key, this.squareSideLength = 0.14}) : super(key: key);

  final double squareSideLength;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double horizMargins =
        (screenWidth - (screenHeight * squareSideLength * 3)) / 2;
    if(MediaQuery.of(context).orientation == Orientation.landscape){
      horizMargins =
      (horizMargins > (screenWidth * 0.05)) ? screenWidth * 0.05 : horizMargins;
    }

    return Container(
        margin: EdgeInsets.fromLTRB(horizMargins, screenHeight * 0.025,
            horizMargins, screenHeight * 0.025),
        child: Column(children: <Widget>[
          Row(children: <Widget>[
            Square(
              xPoint: 0,
              yPoint: 0,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 0,
              yPoint: 1,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 0,
              yPoint: 2,
              sideLength: squareSideLength,
            ),
          ]),
          Row(children: <Widget>[
            Square(
              xPoint: 1,
              yPoint: 0,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 1,
              yPoint: 1,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 1,
              yPoint: 2,
              sideLength: squareSideLength,
            ),
          ]),
          Row(children: <Widget>[
            Square(
              xPoint: 2,
              yPoint: 0,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 2,
              yPoint: 1,
              sideLength: squareSideLength,
            ),
            Square(
              xPoint: 2,
              yPoint: 2,
              sideLength: squareSideLength,
            ),
          ]),
        ]));
  }
}

class Square extends StatelessWidget {
  const Square(
      {Key? key,
      required this.xPoint,
      required this.yPoint,
      this.sideLength = 0.14})
      : super(key: key);
  final int xPoint;
  final int yPoint;
  final double sideLength;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.height * sideLength,
        height: MediaQuery.of(context).size.height * sideLength,
        child: OutlinedButton(
            onPressed: () {
              if (Provider.of<GameModel>(context, listen: false)
                      .getMarkByCoord(xPoint, yPoint) !=
                  "") {
                return;
              } else {
                bool flag = false;
                String mark = Provider.of<GameModel>(context, listen: false)
                        .playerMarkType
                    ? "X"
                    : "O";
                Provider.of<GameModel>(context, listen: false)
                    .makePlayerMove(xPoint, yPoint);
                Provider.of<GameModel>(context, listen: false).log(
                    "Log info: $mark go to [" +
                        xPoint.toString() +
                        ";" +
                        yPoint.toString() +
                        "]");
                Provider.of<GameModel>(context, listen: false)
                    .changeMarkTypeToMove();
                String isWinVar =
                    Provider.of<GameModel>(context, listen: false).isWin();
                if (isWinVar != "") {
                  flag = true;
                } else {
                  mark = Provider.of<GameModel>(context, listen: false)
                          .isMarkToMoveX()
                      ? "X"
                      : "O";
                  Map<int, int> computerMoveCoord =
                      Provider.of<GameModel>(context, listen: false)
                          .makeComputerMove();
                  Provider.of<GameModel>(context, listen: false).log(
                      "Log info: $mark go to [" +
                          computerMoveCoord.keys.elementAt(0).toString() +
                          ";" +
                          computerMoveCoord.values.elementAt(0).toString() +
                          "]");
                  isWinVar =
                      Provider.of<GameModel>(context, listen: false).isWin();
                  if (isWinVar != "") {
                    flag = true;
                  }
                }

                if (flag) {
                  afterEndGameActions(isWinVar, context);
                } else {
                  Provider.of<GameModel>(context, listen: false)
                      .changeMarkTypeToMove();
                }
              }
            },
            style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)),
                side: const BorderSide(
                    width: 2.0, color: Color.fromRGBO(172, 0, 0, 1.0)),
                backgroundColor: const Color.fromRGBO(246, 238, 238, 1.0)),
            child: Align(
              alignment: Alignment.center,
              child: Consumer<GameModel>(builder: (context, game, child) {
                return AutoSizeText(game.getMarkByCoord(xPoint, yPoint),
                  style: const TextStyle(
                      fontSize: 100, color: Color.fromRGBO(27, 29, 44, 1.0)),
                );
              }),
            )));
  }
}

void afterEndGameActions(String isWinVar, BuildContext context) {
  Provider.of<GameModel>(context, listen: false)
      .log("Log info: $isWinVar won!!!");
  bool playerMarkType =
      Provider.of<GameModel>(context, listen: false).playerMarkType;
  bool isPlayerWin = (isWinVar == (playerMarkType ? "X" : "O"));
  String dialogMessage;
  if (isWinVar == "D") {
    dialogMessage = "Friendship won";
  } else {
    dialogMessage = (isPlayerWin) ? "You won!!!" : "You lost(((";
  }

  updateStatisticRequest(
      Provider.of<GameModel>(context, listen: false).username,
      isPlayerWin,
      (isWinVar == "D"),
      playerMarkType);
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor: const Color.fromRGBO(104, 67, 15, 1.0),
          children: <Widget>[
            Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                  Text(dialogMessage,
                      style: const TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(246, 238, 238, 1.0))),
                  Container(
                      margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                      child: OutlinedButton(
                          onPressed: () {
                            Navigator.popUntil(
                                context, ModalRoute.withName('/account'));
                          },
                          style: OutlinedButton.styleFrom(
                              minimumSize: const Size(100, 40),
                              maximumSize: const Size(150, 50),
                              backgroundColor:
                                  const Color.fromRGBO(27, 29, 44, 1.0)),
                          child: const Center(
                            child: Text('Back to account',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color.fromRGBO(246, 238, 238, 1.0))),
                          )))
                ]))
          ],
        );
      });
}
