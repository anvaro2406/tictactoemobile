import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'httpRequests.dart';
import 'drawer.dart';

class FormattedStandardText extends StatelessWidget {
  const FormattedStandardText({Key? key, required this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: const EdgeInsets.fromLTRB(5, 12.5, 5, 12.5),
          child: Text(text,
              style: const TextStyle(
                  fontSize: 21, color: Color.fromRGBO(246, 238, 238, 1.0))),
        ));
  }
}

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  late Future<Statistic> futureStatistic;
  late String username;

  @override
  Widget build(BuildContext context) {
    username = ModalRoute.of(context)!.settings.arguments as String;
    futureStatistic = getStatisticRequest(username);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return FutureBuilder<Statistic>(
      future: futureStatistic,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              key: _key,
              drawer: SignInDrawer(
                username: username,
              ),
              body: OrientationBuilder(builder: (context, orientation) {
                if (orientation == Orientation.portrait) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            margin: const EdgeInsets.fromLTRB(0, 0, 0, 30.0),
                            child: Center(
                              child: Text(username + "'s account",
                                  style: GoogleFonts.playfairDisplay(
                                      textStyle: const TextStyle(
                                          fontSize: 36,
                                          fontWeight: FontWeight.bold,
                                          color: Color.fromRGBO(
                                              246, 238, 238, 1.0)))),
                            )),
                        FormattedStandardText(
                            text: 'Total number of played games: ' +
                                snapshot.data!.totalGames.toString()),
                        FormattedStandardText(
                            text: 'Number of winning games: ' +
                                snapshot.data!.winnings.toString()),
                        FormattedStandardText(
                            text: 'Number of losing games: ' +
                                snapshot.data!.losings.toString()),
                        FormattedStandardText(
                            text: 'Number of draw games: ' +
                                snapshot.data!.draws.toString()),
                        FormattedStandardText(
                            text: 'Number of games, played for O: ' +
                                snapshot.data!.playedForO.toString()),
                        FormattedStandardText(
                            text: 'Number of games, played for X: ' +
                                snapshot.data!.playedForX.toString()),
                        Container(
                            margin: EdgeInsets.fromLTRB(screenWidth * 0.025,
                                screenHeight * 0.025, screenWidth * 0.025, 0),
                            child: OutlinedButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                    context,
                                    "/game",
                                    arguments: username,
                                  ).then((value) => setState(() {}));
                                },
                                style: OutlinedButton.styleFrom(
                                    minimumSize: Size(screenWidth * 0.30,
                                        screenHeight * 0.03),
                                    maximumSize: Size(screenWidth * 0.45,
                                        screenHeight * 0.075),
                                    backgroundColor:
                                        const Color.fromRGBO(27, 29, 44, 1.0)),
                                child: const Center(
                                  child: Text('Play with computer',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Color.fromRGBO(
                                              246, 238, 238, 1.0))),
                                ))),
                        Container(
                            margin: EdgeInsets.fromLTRB(screenWidth * 0.025,
                                screenHeight * 0.025, screenWidth * 0.025, 0),
                            child: OutlinedButton(
                                onPressed: () {
                                  Navigator.popUntil(
                                      context, ModalRoute.withName('/'));
                                },
                                style: OutlinedButton.styleFrom(
                                    minimumSize: Size(screenWidth * 0.30,
                                        screenHeight * 0.03),
                                    maximumSize: Size(screenWidth * 0.45,
                                        screenHeight * 0.075),
                                    backgroundColor:
                                        const Color.fromRGBO(27, 29, 44, 1.0)),
                                child: const Center(
                                  child: Text('Sign out',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Color.fromRGBO(
                                              246, 238, 238, 1.0))),
                                )))
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                              child: Center(
                                child: Text(username + "'s account",
                                    style: GoogleFonts.playfairDisplay(
                                        textStyle: const TextStyle(
                                            fontSize: 36,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromRGBO(
                                                246, 238, 238, 1.0)))),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FormattedStandardText(
                                      text: 'Total number of played games: ' +
                                          snapshot.data!.totalGames.toString()),
                                  FormattedStandardText(
                                      text: 'Number of winning games: ' +
                                          snapshot.data!.winnings.toString()),
                                  FormattedStandardText(
                                      text: 'Number of losing games: ' +
                                          snapshot.data!.losings.toString()),
                                  Container(
                                      margin: EdgeInsets.fromLTRB(
                                          screenWidth * 0.025,
                                          screenHeight * 0.025,
                                          screenWidth * 0.025,
                                          0),
                                      child: OutlinedButton(
                                          onPressed: () {
                                            Navigator.pushNamed(
                                              context,
                                              "/game",
                                              arguments: username,
                                            ).then((value) => setState(() {}));
                                          },
                                          style: OutlinedButton.styleFrom(
                                              minimumSize: Size(
                                                  screenWidth * 0.1,
                                                  screenHeight * 0.075),
                                              maximumSize: Size(
                                                  screenWidth * 0.2,
                                                  screenHeight * 0.15),
                                              backgroundColor:
                                                  const Color.fromRGBO(
                                                      27, 29, 44, 1.0)),
                                          child: const Center(
                                            child: Text('Play with computer',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    color: Color.fromRGBO(
                                                        246, 238, 238, 1.0))),
                                          ))),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FormattedStandardText(
                                      text: 'Number of draw games: ' +
                                          snapshot.data!.draws.toString()),
                                  FormattedStandardText(
                                      text: 'Number of games, played for O: ' +
                                          snapshot.data!.playedForO.toString()),
                                  FormattedStandardText(
                                      text: 'Number of games, played for X: ' +
                                          snapshot.data!.playedForX.toString()),
                                  Container(
                                      margin: EdgeInsets.fromLTRB(
                                          screenWidth * 0.025,
                                          screenHeight * 0.025,
                                          screenWidth * 0.025,
                                          0),
                                      child: OutlinedButton(
                                          onPressed: () {
                                            Navigator.popUntil(context,
                                                ModalRoute.withName('/'));
                                          },
                                          style: OutlinedButton.styleFrom(
                                              minimumSize: Size(
                                                  screenWidth * 0.1,
                                                  screenHeight * 0.075),
                                              maximumSize: Size(
                                                  screenWidth * 0.2,
                                                  screenHeight * 0.15),
                                              backgroundColor:
                                                  const Color.fromRGBO(
                                                      27, 29, 44, 1.0)),
                                          child: const Center(
                                            child: Text('Sign out',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    color: Color.fromRGBO(
                                                        246, 238, 238, 1.0))),
                                          )))
                                ],
                              ),
                            ],
                          )
                        ]),
                  );
                }
              }),
              floatingActionButton: OutlinedButton(
                style: OutlinedButton.styleFrom(
                    minimumSize: const Size(50, 50),
                    backgroundColor: const Color.fromRGBO(27, 29, 44, 1.0)),
                onPressed: () {
                  _key.currentState!.openDrawer();
                },
                child: const Icon(
                  Icons.navigation_outlined,
                  color: Color.fromRGBO(246, 238, 238, 1.0),
                  size: 30,
                ),
              ),
              backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
        } else if (snapshot.hasError) {
          return Scaffold(
              body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                    SizedBox(
                      width: screenWidth * 0.8,
                      child: Text('${snapshot.error}',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(246, 238, 238, 1.0))),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(screenWidth * 0.025,
                            screenHeight * 0.025, screenWidth * 0.025, 0),
                        child: OutlinedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            style: OutlinedButton.styleFrom(
                                minimumSize: Size(
                                    screenWidth * 0.30, screenHeight * 0.025),
                                maximumSize: Size(
                                    screenWidth * 0.45, screenHeight * 0.075),
                                backgroundColor:
                                    const Color.fromRGBO(27, 29, 44, 1.0)),
                            child: const Center(
                              child: Text('Back',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color:
                                          Color.fromRGBO(246, 238, 238, 1.0))),
                            )))
                  ])),
              backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0));
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
