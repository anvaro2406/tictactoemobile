import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<String> signUpRequest(String username, String password) async {
  final response = await http.post(Uri.parse(
      'http://tictactoe-env.eba-cpjirbmb.eu-central-1.elasticbeanstalk.com'
          '/api/sign_up?username=' +
          username +
          '&password=' +
          password));
  if (response.body == "") {
    return "";
  } else {
    return jsonDecode(response.body)["message"];
  }
}

Future<String> signInRequest(String username, String password) async {
  final response = await http.post(Uri.parse(
      'http://tictactoe-env.eba-cpjirbmb.eu-central-1.elasticbeanstalk.com'
          '/api/sign_in?username=' +
          username +
          '&password=' +
          password));
  if (jsonDecode(response.body)["code"] != 450) {
    return "";
  } else {
    return jsonDecode(response.body)["message"];
  }
}

Future<Statistic> getStatisticRequest(String username) async {
  final response = await http.get(Uri.parse(
      'http://tictactoe-env.eba-cpjirbmb.eu-central-1.elasticbeanstalk.com'
          '/api/get_statistic?username=' +
          username));
  debugPrint("CAPSLOCK");
  if (jsonDecode(response.body)["code"] != 450) {
    return Statistic.fromJson(jsonDecode(response.body));
  } else {
    throw Exception(jsonDecode(response.body)["message"]);
  }
}

Future<http.Response> updateStatisticRequest(String username, bool isWinning, bool isDraw, bool isPlayedForX) {
  return http.put(
    Uri.parse('http://tictactoe-env.eba-cpjirbmb.eu-central-1.elasticbeanstalk.com/api/update_statistic'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      'username': username,
      'isWinning': isWinning,
      'isDraw': isDraw,
      'isPlayedForX': isPlayedForX,
    }),
  );
}

class Statistic {
  final int id;
  final int totalGames;
  final int winnings;
  final int losings;
  final int draws;
  final int playedForO;
  final int playedForX;

  const Statistic({
    required this.id,
    required this.totalGames,
    required this.winnings,
    required this.losings,
    required this.draws,
    required this.playedForO,
    required this.playedForX,
  });

  factory Statistic.fromJson(Map<String, dynamic> json) {
    return Statistic(
      id: json['id'],
      totalGames: json['total_games'],
      winnings: json['winnings'],
      losings: json['losings'],
      draws: json['draws'],
      playedForO: json['played_for_o'],
      playedForX: json['played_for_x'],
    );
  }
}
