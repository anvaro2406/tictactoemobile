import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StartDrawer extends StatelessWidget {
  const StartDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0),
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Color.fromRGBO(27, 29, 44, 1.0),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text('Start menu',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.playfairDisplay(
                      textStyle: const TextStyle(
                          fontSize: 36,
                          color: Color.fromRGBO(246, 238, 238, 1.0)))),
            ),
          ),
          ListTile(
            title: const Text('Sign up page',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.pushNamed(context, '/sign_up');
            },
          ),
          ListTile(
            title: const Text('Sign in page',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.pushNamed(context, '/sign_in');
            },
          ),
          ListTile(
            title: const Text('Start page',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          ),
        ],
      ),
    );
  }
}

class SignInDrawer extends StatelessWidget {
  const SignInDrawer({Key? key, required this.username}) : super(key: key);
  final String username;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: const Color.fromRGBO(42, 43, 50, 1.0),
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Color.fromRGBO(27, 29, 44, 1.0),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text('Sign in menu',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.playfairDisplay(
                      textStyle: const TextStyle(
                          fontSize: 36,
                          color: Color.fromRGBO(246, 238, 238, 1.0)))),
            ),
          ),
          ListTile(
            title: const Text('Account page',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.pushNamed(
                context,
                '/account',
                arguments: username,
              );
            },
          ),
          ListTile(
            title: const Text('Game page',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.pushNamed(
                context,
                '/game',
                arguments: username,
              );
            },
          ),
          ListTile(
            title: const Text('Sign out',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22, color: Color.fromRGBO(246, 238, 238, 1.0))),
            onTap: () {
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ],
      ),
    );
  }
}
